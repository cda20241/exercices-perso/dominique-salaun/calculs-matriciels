.. calculs-matriciels documentation master file, created by
   sphinx-quickstart on Wed Oct  4 13:04:38 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans la documentation du calculs-matriciels's !
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
