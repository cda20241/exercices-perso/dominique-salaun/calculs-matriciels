import unittest
from main import matrice, change_valeur, matrice_identite, dimension_check, somme_matrice, is_element, produit_scalaire, produit_matriciel, MatriceDimensionError

class TestMatriceFunctions(unittest.TestCase):

    def test_matrice(self):
        # Test de la fonction matrice
        nb_lignes = 3
        nb_colonnes = 2
        result = matrice(nb_lignes, nb_colonnes)
        self.assertEqual(len(result), nb_lignes)
        self.assertEqual(len(result[0]), nb_colonnes)

    def test_change_valeur(self):
        # Test de la fonction change_valeur
        ma_matrice = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]
        ligne = 1
        colonne = 2
        nouvelle_valeur = 42
        result = change_valeur(ma_matrice, ligne, colonne, nouvelle_valeur)
        self.assertEqual(result[ligne][colonne], nouvelle_valeur)

    def test_matrice_identite(self):
        # Test de la fonction matrice_identite
        taille = 4
        result = matrice_identite(taille)
        self.assertEqual(len(result), taille)
        for i in range(taille):
            self.assertEqual(len(result[i]), taille)
            self.assertEqual(result[i][i], 1)

    def test_dimension_check(self):
        # Test de la fonction dimension_check
        matrice1 = [
            [1, 2, 3],
            [4, 5, 6]
        ]
        matrice2 = [
            [7, 8, 9],
            [10, 11, 12]
        ]
        matrice3 = [
            [1, 2],
            [3, 4],
            [5, 6]
        ]
        self.assertTrue(dimension_check(matrice1, matrice2))
        self.assertFalse(dimension_check(matrice1, matrice3))

    def test_somme_matrice(self):
        # Test de la fonction somme_matrice
        matrice1 = [
            [1, 2, 3],
            [4, 5, 6]
        ]
        matrice2 = [
            [7, 8, 9],
            [10, 11, 12]
        ]
        result = somme_matrice(matrice1, matrice2)
        expected_result = [
            [8, 10, 12],
            [14, 16, 18]
        ]
        self.assertEqual(result, expected_result)

    def test_is_element(self):
        # Test de la fonction is_element
        ma_matrice = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]
        element_a_rechercher = 5
        position = is_element(ma_matrice, element_a_rechercher)
        self.assertEqual(position, (1, 1))

    def test_produit_scalaire(self):
        # Test de la fonction produit_scalaire
        ma_matrice = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]
        facteur = 2.5
        result = produit_scalaire(ma_matrice, facteur)
        expected_result = [
            [2.5, 5.0, 7.5],
            [10.0, 12.5, 15.0],
            [17.5, 20.0, 22.5]
        ]
        self.assertEqual(result, expected_result)

    def test_produit_matriciel(self):
        # Test de la fonction produit_matriciel
        matrice1 = [
            [1, 2],
            [3, 4]
        ]
        matrice2 = [
            [5, 6],
            [7, 8]
        ]
        result = produit_matriciel(matrice1, matrice2)
        expected_result = [
            [19, 22],
            [43, 50]
        ]
        self.assertEqual(result, expected_result)

if __name__ == '__main__':
    unittest.main()