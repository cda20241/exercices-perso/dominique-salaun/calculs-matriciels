class MatriceDimensionError(Exception):
    pass

def matrice(nb_lignes, nb_colonnes):
    mat = []

    for i in range(nb_lignes):
        ligne = []
        for j in range(nb_colonnes):
            ligne.append(0)
        mat.append(ligne)

    return mat

def change_valeur(matrice, ligne, colonne, nouvelle_valeur):
    if 0 <= ligne < len(matrice) and 0 <= colonne < len(matrice[0]):
        matrice[ligne][colonne] = nouvelle_valeur
    else:
        print("Indices de ligne(s) ou de colonne(s) non valides.")

    return matrice

def matrice_identite(taille):
    mat_identite = []

    for i in range(taille):
        ligne = []
        for j in range(taille):
            if i == j:
                ligne.append(1)
            else:
                ligne.append(0)
        mat_identite.append(ligne)

    return mat_identite

def print_matrice(matrice):
    for ligne in matrice:
        for valeur in ligne:
            print(valeur, end="\t")
        print()

def dimension_check(matrice1, matrice2):
    lignes_mat1 = len(matrice1)
    colonnes_mat1 = len(matrice1[0]) if lignes_mat1 > 0 else 0

    lignes_mat2 = len(matrice2)
    colonnes_mat2 = len(matrice2[0]) if lignes_mat2 > 0 else 0

    if lignes_mat1 == lignes_mat2 and colonnes_mat1 == colonnes_mat2:
        return True
    else:
        return False

def somme_matrice(matrice1, matrice2):
    if len(matrice1) != len(matrice2) or len(matrice1[0]) != len(matrice2[0]):
        raise MatriceDimensionError("Les matrices n'ont pas la même dimension.")

    somme = []

    for i in range(len(matrice1)):
        ligne = []
        for j in range(len(matrice1[0])):
            valeur = matrice1[i][j] + matrice2[i][j]
            ligne.append(valeur)
        somme.append(ligne)

    return somme

def is_element(matrice, element):
    for x, ligne in enumerate(matrice):
        for y, valeur in enumerate(ligne):
            if valeur == element:
                return x, y
    return None

def produit_scalaire(matrice, nombre):
    resultat = []

    for ligne in matrice:
        nouvelle_ligne = [valeur * nombre for valeur in ligne]
        resultat.append(nouvelle_ligne)

    return resultat

def produit_matriciel(matrice1, matrice2):
    lignes_mat1 = len(matrice1)
    colonnes_mat1 = len(matrice1[0]) if lignes_mat1 > 0 else 0

    lignes_mat2 = len(matrice2)
    colonnes_mat2 = len(matrice2[0]) if lignes_mat2 > 0 else 0

    if colonnes_mat1 != lignes_mat2:
        raise MatriceDimensionError("Les dimensions des matrices ne sont pas cohérentes pour la multiplication matricielle.")

    resultat = []

    for i in range(lignes_mat1):
        ligne_resultat = []
        for j in range(colonnes_mat2):
            somme = 0
            for k in range(colonnes_mat1):
                somme += matrice1[i][k] * matrice2[k][j]
            ligne_resultat.append(somme)
        resultat.append(ligne_resultat)

    return resultat
